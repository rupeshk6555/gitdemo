**1. create a sample project in GitLab.**
![Screenshot_2022-12-04_113510](/uploads/dcd2d24929d3f1676a21852a0e3b18a7/Screenshot_2022-12-04_113510.png)
**2. Clone the repository**

- On your project page, select Clone and Copy the URL for Clone with https.
  ![Screenshot_2022-12-04_113628](/uploads/31f65bf98b4495eefa22e1f42074c29b/Screenshot_2022-12-04_113628.png)
- Open a terminal on your computer  
  ![Screenshot_2022-12-04_114006](/uploads/598b489ab8332dfc9d862973f71d438c/Screenshot_2022-12-04_114006.png)
- Enter git clone and paste the URL:
  `git clone https://gitlab.com/rupeshk6555/gitdemo.git`
  ![Screenshot_2022-12-04_114040](/uploads/ef0b58bcf54e023d380308ee959d17fb/Screenshot_2022-12-04_114040.png)
- Go to the directory:
  `cd gitdemo`
  ![Screenshot_2022-12-04_114139](/uploads/1c61c70ec82490e5892d1a70b585f7ba/Screenshot_2022-12-04_114139.png)

3. Commit and push your file

- 1. Add the `node_modules` files to the staging area. `git add.`

![Screenshot_2022-12-04_115100](/uploads/519c29e29d1bfc62118c4e41a1b9cb5a/Screenshot_2022-12-04_115100.png)

- 2. Confirm the file is staged.
     `git status`![Screenshot_2022-12-04_115236](/uploads/19438d2b2a2416b8eaf04c6750d1e1f5/Screenshot_2022-12-04_115236.png)

- 3. Now commit the staged file
     `git commit -m "I added node modules file"`
     ![Screenshot_2022-12-04_115332](/uploads/1dce578feaff5f8ec34d058d6f944a29/Screenshot_2022-12-04_115332.png)
- 4. Push your file to GitLab.
     ` git push origin`
     ![Screenshot_2022-12-04_115412](/uploads/a7f111794c8814a42c68be9aecc4c560/Screenshot_2022-12-04_115412.png)

![Screenshot_2022-12-04_115453](/uploads/379dca64971b3fae9d2486ebea50eb1f/Screenshot_2022-12-04_115453.png)
![Screenshot_2022-12-04_115528](/uploads/6157784dc27cef6ce29f63ece8efb2f6/Screenshot_2022-12-04_115528.png)

**git pull**

1.Create a new repository on Gitlab.
![Screenshot_2022-12-04_124423](/uploads/e7d287ec8167f2fff8dfac1ab42248f2/Screenshot_2022-12-04_124423.png)
![Screenshot_2022-12-04_134355](/uploads/6e54a4847e121f626550d691eef8c649/Screenshot_2022-12-04_134355.png)
2.Open terminal and move to that folder

       cd FOLDER

![Screenshot_2022-12-04_134546](/uploads/064d1af640486bc68b6c83c8301262ef/Screenshot_2022-12-04_134546.png)

3.Add the remote URL as origin

      git remote add origin https://gitlab.com/rupeshk6555/gitdemo.git

![Screenshot_2022-12-04_135733](/uploads/562b2fd61e141b606a64d7f167fe4819/Screenshot_2022-12-04_135733.png)

4.Now using the pull command, you can ‘pull’ down the README file onto the local folder

      git pull origin master

![Screenshot_2022-12-04_135814](/uploads/78da7b3c414840985cafbc3de0695b48/Screenshot_2022-12-04_135814.png)

